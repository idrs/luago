userdata = userdata or {}
moduledata = moduledata or {}
moduledata.luago = {}
lg = moduledata.luago

math.randomseed(1234)

--  variables
lg.gobans = {}
lg.cur = 1
lg.fuz = 0.1


lg.gobans[lg.cur] = {size = 19,stones = {}}

function lg.str2tab(str)
	local t = {}
	for i in string.gmatch(str, "[^,%s]+") do
		table.insert(t,i)
	end
	return t
end

notationString =   ABCDEFGHJKLMNOPQRSTUVWXYZ"

function readNotation(str)
	x = notationString:find(str:sub(1,1))
	y = tonumber(str:sub(2,3))
	return x,y
end
	
function rand()
	return math.random()*2-1
end

	
function addStone(pos,col,symbol)
	x,y = readNotation(pos)
	local insert = true
	for stone in ipairs(lg.gobans[lg.cur].stones) do
		if stone.pos == pos then
			if col == "symbol" then
				stone.symbol = symbol
			else
				stone.col = col
				stone.symbol = symbol
			end
			insert = false
		end
	end
	if insert then
		table.insert(lg.gobans[lg.cur].stones,
			{
				pos = pos,
				x 	= x+lg.fuz*rand(),
				y = y+lg.fuz*rand(),
				c = col,
				symbol = symbol
			})
	end
end

function lg.gobanSize(size)
	lg.gobans[cur].size = size
end

function lg.useGoban(numGoban)
	lg.cur = numGoban
end


function lg.showGoban(startpos,endpos)
	local goban = lg.gobans[lg.cur];
	local size = goban.size
	local stones = goban.stones
	-- defining part of the goban to show
	if startpos and endpos then
		xmin,ymin = readNotation(startpos)
		xmax,ymax = readNotation(endpos)
		if xmin>xmax then
			tmp = xmax
			xmax = xmin
			xmin = tmp
		end
		if ymin > ymax then
			tmp = ymax
			ymax = ymin
			ymin = tmp
		end
	else
		xmin = size
		ymin = size
		xmax = 0
		ymax = 0
		for _,stone in pairs(stones) do
			x,y = readNotation(stone.pos)
			xmin = math.min(x,xmin)
			ymin = math.min(y,ymin)
			xmax = math.max(x,xmax)
			ymax = math.max(y,ymax)
		end
		if xmin<3 then xmin = 1 end
		if ymin<3 then ymin = 1 end
		if xmax>size-3 then xmax = size end
		if ymax>size-3 then ymax = size end
	end
	xmin = math.max(1,xmin-1) -0.5
	ymin = math.max(1,ymin-1) - 0.5
	xmax = math.min(size,xmax+1) + 0.5
	ymax = math.min(size,ymax+1)  + 0.5
	
	-- drawing goban
	tex.print("\\begin{tikzpicture}")
	tex.print("\\clip("..xmin..","..ymin..")rectangle("..xmax..","..ymax..");")
	tex.print("\\draw[line width=1.5pt](1,1)--("..size..",1)--("..size..","..size..")--(1,"..size..")--cycle;")
	tex.print("\\draw[step=1,black] (1,1) grid ("..size..","..size..");")
	
	-- star points
	if size == 19 then
		for i=0,2 do
			for j=0,2 do
				x = 4+i*6
				y = 4+j*6
				tex.print("\\draw[fill=black]("..x..","..y..")circle(0.1);")
			end
		end
	elseif size == 13 then
		for i=0,1 do
			for j=0,1 do
				x = 4+i*6
				y = 4+j*6
				tex.print("\\draw[fill=black]("..x..","..y..")circle(0.1);")
			end
		end
		tex.print("\\draw[fill=black](7,7)circle(0.1);")
	elseif size == 9 then
		for i=0,1 do
			for j=0,1 do
				x = 3+i*5
				y = 3+j*5
				tex.print("\\draw[fill=black]("..x..","..y..")circle(0.1);")
			end
		end
		tex.print("\\draw[fill=black](7,7)circle(0.1);")
	end
	
	-- drawing stones
	tex.print("\\begin{LARGE}")  --change in function of number
	for _,stone in pairs(stones) do
		drawStone(stone.x,stone.y,stone.c,stone.symbol)
	end
	tex.print("\\end{LARGE}")
	
	-- closing picture
	tex.print("\\end{tikzpicture}")
end

function lg.showFullGoban()
	local size = lg.gobans[lg.cur].size;
	lg.showGoban("a1",notationString:sub(size,size)..size)
end

function drawStone(x,y,col,symbol)
	tex.print("\\begin{scope}[shift={(".. x .. ","..y..")}]")
	if col == "symbol" then
		tex.print("\\draw[color=white, fill=white](0,0)circle(0.40)node{".. symbol .."};")
	if col == "white" then
		oth = "black"
		tex.print("\\whitestone")
	else
		oth = "white"
		tex.print("\\blackstone")
	end
	-- symbols
	local stoneradius = 0.45
	local xt = -0.5*stoneradius						--math.cos(-math.pi/3)
	local yt = -math.sqrt(3)/2*stoneradius 	--math.sin(-math.pi/3)
	local xc = math.sqrt(2)/2 * stoneradius
	local symbols = {}
	symbols["triangle"]="\\draw[line width = 1pt,color ="..oth.."](0,0.45)--(".. xt ..",".. yt..")--(".. -xt..","..yt ..")--cycle;"
	symbols["square"] = "\\draw[line width = 1pt,color ="..oth.."]("..xc ..","..xc ..")--(".. -xc ..",".. xc..")--(".. -xc..","..-xc ..")--(".. xc ..",".. -xc..")--cycle;"
	symbols["circle"] = "\\draw[line width = 1pt,color ="..oth.."](0,0)circle(0.25);";
	symbols["none"] = ""
	
		--tex.print("\\draw[line width=1.5pt, fill="..col.."]("..x..","..y..")circle(0.45);")
	if type(symbol) == "number" then
		if symbol<10 then
			sizefont = "\\Huge"
		elseif symbol <100 then
			sizefont = "\\LARGE"
		else
			sizefont = "\\Large"
		end
		tex.print("\\draw(0,0)node[color="..oth.."]{"..sizefont.." "..symbol.."};")
	else
		tex.print(symbols[symbol])
	end
	tex.print{"\\end{scope}"}
end

function lg.white(t,symbol)
	if type(tonumber(symbol))=="number" then
		num = tonumber(symbol)
		col = "white"
		oth = "black"
		for k,v in ipairs(t) do
			addStone(v,col,num)
			col,oth = oth,col
			num = num+1
		end
	elseif symbol == "none" then
		col = "white"
		oth = "black"
		for k,v in ipairs(t) do
			addStone(v,col,"none")
			col,oth = oth,col
		end
	else
		for k,v in ipairs(t) do
			addStone(v,"white",symbol)
		end
	end
end

function lg.black(t,symbol)
	if type(tonumber(symbol))=="number" then
		num = tonumber(symbol)
		col = "black"
		oth = "white"
		for k,v in ipairs(t) do
			addStone(v,col,num)
			col,oth = oth,col
			num = num+1
		end
	elseif symbol == "none" then
		col = "black"
		oth = "white"
		for k,v in ipairs(t) do
			addStone(v,col,"none")
			col,oth = oth,col
		end
	else
		for k,v in ipairs(t) do
			addStone(v,"black",symbol)
		end
	end
end

function lg.gobanSymbol(pos,symbol)
	tmp = lg.fuzz
	lg.fuzz = 0
	addStone(pos,"symbol",symbol)
	lg.fuzz = tmp
end

function lg.clear(pos)
	for k,stone in ipairs(lg.gobans[lg.cur].stones) do
		if stone.pos == pos then
			table.remove(lg.gobans[lg.cur].stones,k)
			break
		end
	end
end

function lg.clearGobanSymbols()
	local goban = lg.gobans[lg.cur]
	for _,stone in ipairs(goban.stones) do
		stone.symbol = "none"
	end
	goban.symbols = {}
end


function lg.copyToGoban(numGoban)

end

function lg.copyFromGoban(numGoban)

end

function lg.rotateGoban()

end

function lg.rotateGobanLeft()

end

function lg.rotateGobanRight()

end

function lg.hflipGoban()

end

function lg.vflipGoban()

end


function lg.mirrorGoban()

end