local re = require"re"

local luatoxml = require".luatoxml"

parserre = re.compile([[
collection <- {| gametree+ |}
gametree <- space "(" node+ space ")" space
node <-  space ";"  {|{:type: ''->'node' :} property* {:nx: (node / variation)* :}|} -> reformat
variation <- space {|{:type: ''->'variation' :} ("(" node space ")")+ |} space
property <- space {|{:id:  propident :} space ("[" value  "]" space)+ |} space
propident <- [A-Z]+
value <-  space { [^][]* (space [^][%s] )*}  
space <- %s*
]],{["reformat"]=
	function(t) 
		local nt={}
		for _,v in ipairs(t) do
			id = v.id
			v.id = nil
			nt[id]=v
		end
		nt.nx = t.nx
		nt.type = t.type
		return nt
	end
})


parserre:match("(;BP[aa][cd]WP[zz])")
	
--[[  program   = Ct(V'sexpr' ^ 0),
  wspace    = S' \n\r\t' ^ 0,
  atom      = V'boolean' + V'integer' + V'string' + V'symbol',
    symbol  = C((1 - S' \n\r\t"\'()[]{}#@~') ^ 1) /
              function(s) return _G[s] end,
    boolean = C(P'true' + P'false') /
              function(x) return x == 'true' end,
    integer = C(R'19' * R'09' ^ 0) /
              tonumber,
    string  = P'"' * C((1 - S'"\n\r') ^ 0) * P'"',
  coll      = V'list' + V'array',
    list    = P'\'(' * Ct(V'expr' ^ 1) * P')',
    array   = P'[' * Ct(V'expr' ^ 1) * P']',
  expr      = V'wspace' * (V'coll' + V'atom' + V'sexpr'),
  sexpr     = V'wspace' * P'(' * V'symbol' * Ct(V'expr' ^ 0) * P')' /
              function(f, ...) return f(...) end
}
]]--

sgfexample =
[[(;FF[4]AP[Primiview:3.1]GM[1]SZ[19]GN[Gametree 1: properties]US[Arno Hollosi]

(;B[pd]N[Moves, comments, annotations]
C[Nodename set to: "Moves, comments, annotations"];W[dp]GW[1]
C[Marked as "Good for White"];B[pp]GB[2]
C[Marked as "Very good for Black"];W[dc]GW[2]
C[Marked as "Very good for White"];B[pj]DM[1]
C[Marked as "Even position"];W[ci]UC[1]
C[Marked as "Unclear position"];B[jd]TE[1]
C[Marked as "Tesuji" or "Good move"];W[jp]BM[2]
C[Marked as "Very bad move"];B[gd]DO[]
C[Marked as "Doubtful move"];W[de]IT[]
C[Marked as "Interesting move"];B[jj];
C[White "Pass" move]W[];
C[Black "Pass" move]B[tt])

(;AB[dd][de][df][dg][do:gq]
  AW[jd][je][jf][jg][kn:lq][pn:pq]
N[Setup]C[Black & white stones at the top are added as single stones.

Black & white stones at the bottom are added using compressed point lists.]
;AE[ep][fp][kn][lo][lq][pn:pq]
C[AddEmpty

Black stones & stones of left white group are erased in ff3 way.

White stones at bottom right were erased using compressed point list.]
;AB[pd]AW[pp]PL[B]C[Added two stones.

Node marked with "Black to play".];PL[W]
C[Node marked with "White to play"])

(;AB[dd][de][df][dg][dh][di][dj][nj][ni][nh][nf][ne][nd][ij][ii][ih][hq]
[gq][fq][eq][dr][ds][dq][dp][cp][bp][ap][iq][ir][is][bo][bn][an][ms][mr]
AW[pd][pe][pf][pg][ph][pi][pj][fd][fe][ff][fh][fi][fj][kh][ki][kj][os][or]
[oq][op][pp][qp][rp][sp][ro][rn][sn][nq][mq][lq][kq][kr][ks][fs][gs][gr]
[er]N[Markup]C[Position set up without compressed point lists.]

;TR[dd][de][df][ed][ee][ef][fd:ff]
 MA[dh][di][dj][ej][ei][eh][fh:fj]
 CR[nd][ne][nf][od][oe][of][pd:pf]
 SQ[nh][ni][nj][oh][oi][oj][ph:pj]
 SL[ih][ii][ij][jj][ji][jh][kh:kj]
 TW[pq:ss][so][lr:ns]
 TB[aq:cs][er:hs][ao]
C[Markup at top partially using compressed point lists (for markup on white stones); listed clockwise, starting at upper left:
- TR (triangle)
- CR (circle)
- SQ (square)
- SL (selected points)
- MA ('X')

Markup at bottom: black & white territory (using compressed point lists)]
;LB[dc:1][fc:2][nc:3][pc:4][dj:a][fj:b][nj:c]
[pj:d][gs:ABCDEFGH][gr:ABCDEFG][gq:ABCDEF][gp:ABCDE][go:ABCD][gn:ABC][gm:AB]
[mm:12][mn:123][mo:1234][mp:12345][mq:123456][mr:1234567][ms:12345678]
C[Label (LB property)

Top: 8 single char labels (1-4, a-d)

Bottom: Labels up to 8 char length.]

;DD[kq:os][dq:hs]
AR[aa:sc][sa:ac][aa:sa][aa:ac][cd:cj]
  [gd:md][fh:ij][kj:nh]
LN[pj:pd][nf:ff][ih:fj][kh:nj]
C[Arrows, lines and dimmed points.])

(;B[qd]N[Style & text type]
C[There are hard linebreaks & soft linebreaks.
Soft linebreaks are linebreaks preceeded by '\\' like this one >o\
k<. Hard line breaks are all other linebreaks.
Soft linebreaks are converted to >nothing<, i.e. removed.

Note that linebreaks are coded differently on different systems.

Examples (>ok< shouldn't be split):

linebreak 1 "\\n": >o\
k<
linebreak 2 "\\n\\r": >o\

k<
linebreak 3 "\\r\\n": >o\
k<
linebreak 4 "\\r": >o\
k<]

(;W[dd]N[W d16]C[Variation C is better.](;B[pp]N[B q4])
(;B[dp]N[B d4])
(;B[pq]N[B q3])
(;B[oq]N[B p3])
)
(;W[dp]N[W d4])
(;W[pp]N[W q4])
(;W[cc]N[W c17])
(;W[cq]N[W c3])
(;W[qq]N[W r3])
)

(;B[qr]N[Time limits, captures & move numbers]
BL[120.0]C[Black time left: 120 sec];W[rr]
WL[300]C[White time left: 300 sec];B[rq]
BL[105.6]OB[10]C[Black time left: 105.6 sec
Black stones left (in this byo-yomi period): 10];W[qq]
WL[200]OW[2]C[White time left: 200 sec
White stones left: 2];B[sr]
BL[87.00]OB[9]C[Black time left: 87 sec
Black stones left: 9];W[qs]
WL[13.20]OW[1]C[White time left: 13.2 sec
White stones left: 1];B[rs]
C[One white stone at s2 captured];W[ps];B[pr];W[or]
MN[2]C[Set move number to 2];B[os]
C[Two white stones captured
(at q1 & r1)]
;MN[112]W[pq]C[Set move number to 112];B[sq];W[rp];B[ps]
;W[ns];B[ss];W[nr]
;B[rr];W[sp];B[qs]C[Suicide move
(all B stones get captured)])
)


(;FF[4]AP[Primiview:3.1]GM[1]SZ[19]C[Gametree 2: game-info

Game-info properties are usually stored in the root node.
If games are merged into a single game-tree, they are stored in the node\
 where the game first becomes distinguishable from all other games in\
 the tree.]
;B[pd]
(;PW[W. Hite]WR[6d]RO[2]RE[W+3.5]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[dp]
C[Game-info:
Black: B. Lack, 5d
White: W. Hite, 6d
Place: London
Event: Go Congress
Round: 2
Result: White wins by 3.5])
(;PW[T. Suji]WR[7d]RO[1]RE[W+Resign]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[cp]
C[Game-info:
Black: B. Lack, 5d
White: T. Suji, 7d
Place: London
Event: Go Congress
Round: 1
Result: White wins by resignation])
(;W[ep];B[pp]
(;PW[S. Abaki]WR[1d]RO[3]RE[B+63.5]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[ed]
C[Game-info:
Black: B. Lack, 5d
White: S. Abaki, 1d
Place: London
Event: Go Congress
Round: 3
Result: Balck wins by 63.5])
(;PW[A. Tari]WR[12k]KM[-59.5]RO[4]RE[B+R]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[cd]
C[Game-info:
Black: B. Lack, 5d
White: A. Tari, 12k
Place: London
Event: Go Congress
Round: 4
Komi: -59.5 points
Result: Black wins by resignation])
))]]

--test

if parserre:match(sgfexample) == nil then print("error in parsing sgf example") end

--
local unit  = 50

obj  = {svg = {xmlns = "http://www.w3.org/2000/svg"}}

whitestone = { 
	circle =  { 
		id = "whitestone",
		cx = 0,
		cy = 0,
		r = 0.45*unit,
		fill = "white",
		stroke = "black",
		["stroke-width"]=3,
	}
}

blackstone = { 
	circle =  { 
		id = "blackstone",
		cx = 0,
		cy = 0,
		r = 0.45*unit,
		fill = "black",
		stroke = "black",
		["stroke-width"]=3,
	}
}


function map(func, array)
  local new_array = {}
  for i,v in ipairs(array) do
    new_array[i] = func(v)
  end
  return new_array
end

function notation2table(aa,col,sym)
	-- argument string of 2 chars
	if aa:len() ~= 2 then print("error in notation string")
	else
		x =string.byte(aa,1) - 96
		y = string.byte(aa,2) - 96
		return {x=x,y=y,color=col,symbol=sym}
	end
end

function str2tab(str)
	local t = {}
	for i in string.gmatch(str, "[^,%s]+") do
		table.insert(t,i)
	end
	return t
end

function grid(size,xmin,ymin,xmax,ymax)
	if xmin <= 3 then xmin = 1 else xmin=xmin-0.5 end
	if ymin <= 3 then ymin = 1 else ymin = ymin-0.5 end
	width = xmax - xmin
	height = ymax - ymin
	if width - height > 3 then ymax = ymin + width - 3 end
	if height - width > 3 then xmax = xmin + height -3 end
	if size-xmax <= 3 then xmax = size else xmax = xmax + 0.5 end
	if size-ymax <= 3 then ymax = size else ymax = ymax + 0.5 end	
	local t = {id = "grid"}
	for i = xmin,xmax do
		elt = {
			line = {
				x1 = tostring(unit*i),
				y1 = tostring(unit*ymin),
				x2 = tostring(unit*i),
				y2 = tostring(unit*ymax),
				stroke = "black",
				["stroke-width"]=2,
				opacity = "0.7",
			}
		}
		if i==1 or i ==size then elt.line["stroke-width"]=4 
			elt.line["stroke-linecap"]="square"
		 end
		table.insert(t,elt)
		
	end
	for i = ymin,ymax do
		elt = {
			line = {
				y1 = tostring(unit*i),
				x1 = tostring(unit*xmin),
				y2 = tostring(unit*i),
				x2 = tostring(unit*xmax),
				stroke = "black",
				["stroke-width"]=2,
				opacity = "0.7",
			}
		}
		
		if i==1 or i ==size then 
			elt.line["stroke-width"]=4 
			elt.line["stroke-linecap"]="square"
		end
		table.insert(t,elt)
		
	end
	for k = 4,19,6 do
		for j = 4,19,6 do
			if k>=xmin and k <= xmax and j >=ymin and j <= ymax then
			table.insert(t,{circle = {
						cx = unit*k,
						cy = unit*j,
						r = 8,
						fill = "black",
					}}
				)
			end
			
		end
	end
	return t,xmin,ymin,xmax,ymax
end


function sgf2svg(sgf,name)
	local t = {}

	t = parserre:match(sgf)
	margin = 2
	csv = "";
	for k,v in ipairs(t) do
		local svg = {}
		-- size parameter
		size = v.SZ or 19
		-- checking FF, etc ?
		
		stones={}
		for _,n in ipairs(v.AB) do
			table.insert(stones,notation2table(n,"black"))
		end
		for _,n in ipairs(v.AW) do
			table.insert(stones,notation2table(n,"white"))
		end
		
		comment = v.C[1]
		
		xmin,xmax,ymin,ymax = size,0,size,0 
		for _,s in ipairs(stones) do
			xmin = math.min(xmin,s.x)
			xmax = math.max(xmax,s.x)
			ymin = math.min(ymin,s.y)
			ymax = math.max(ymax,s.y)
		end
		svg.id = "tsumego";
		svg.xmlns = "http://www.w3.org/2000/svg"
		svg["xmlns:xlink"]="http://www.w3.org/1999/xlink"
		
		fig = {id="figure"}
		svg.width="auto"
		svg["max-height"] = "500px"
		svg.preserveAspectRation ="xMinYMin meet"
--		svg.style = "stroke-width: 0px; background-color: white;"
		svg[1] = {defs = { whitestone,blackstone}}
		
		gobangrid,xmin,ymin,xmax,ymax  = grid(size,xmin-margin,ymin-margin,xmax+margin,ymax+margin)
		xO = unit*(xmin-0.5)
		yO = unit*(ymin-1.5)
		width = unit*(xmax-xmin+1)
		height = unit*(ymax-ymin+3)
		svg.viewBox = table.concat({xO,yO , width , height }," ")
		svg[2] = {rect = 
			{
				x=xO,
				y=yO,
				width="100%",
				height="100%",
				fill="white" --"rgb(179, 189, 72)"
			}
		}
		fig[1] = {g = gobangrid}
		svg[3] = {text = {
				id = "toplay",
				x = (xmax+xmin-0.5)*unit/2,
				y = unit,
				["font-size"]=unit/2,
				["text-anchor"]="middle",
				"Black to play",
			}
			}
		for _,s in ipairs(stones) do
			table.insert(fig,
				{
					g = {
						transform = "translate("..tostring(unit*s.x).." "..tostring(unit*s.y)..")",
						use = {
							x="0",
							y="0",
							class = "stone",
							["xlink:href"]="#"..s.color.."stone",
							},
						},
					})
		end
		table.insert(svg,{g = {id="shift", transform="translate(0 "..unit..")",
					g = fig}})
		xg = unit*(xmax+0.5)/2
		yg = unit*(ymax+0.5)/2
		table.insert(svg,{script = 
				{type = "text/javascript",
					"var k="..k.."; var xg = "..xg..";var yg = "..yg..";var unit="..unit.."; var vbarray=["..table.concat({xO,yO , width , height },",").."];"..[[
					
					function frac(r){
					return (r-Math.floor(r));
					}
					
					function ownRandom(i){
						seed = Math.floor(Date.now()/(10e6));
						dot = i*12.9898+seed*78.233;
						return frac(Math.sin(dot) * 43758.5453)
					}
						
					function randomInt(m,M){
						return Math.floor(4*Math.random()-2);
					}
					
					stonelist = document.getElementsByClassName("stone");
					xchange = Math.floor(ownRandom(k)*2)==0;
					
					h = 2*(Math.floor(ownRandom(k+1)*2)-0.5);
					v = 2*(Math.floor(ownRandom(k+2)*2)-0.5);
					d = Math.floor(ownRandom(k+3)*2);
					
					for (let elt of stonelist) 
					{
						elt.setAttribute(
						"transform",
						"translate("+randomInt()+" "+randomInt()+")")
						if(xchange){
							color = elt.getAttribute("xlink:href");
							if(color=="#blackstone"){
								elt.setAttribute("xlink:href","#whitestone")}
							else{elt.setAttribute("xlink:href","#blackstone")}
						}
					}
					if(xchange){
						document.getElementById("toplay").innerHTML = "White to play";
						}
					
					transCoef = [h*d,h*(1-d),v*(1-d),v*d,0,0].join(" ");
					var transform = "translate("+(xg*d+yg*(1-d))+" "+(yg*d+xg*(1-d))+") matrix("+transCoef+") translate("+(-xg)+" "+(-yg)+")";
					document.getElementById("figure").setAttribute("transform",transform);
					if(d==0){
						svg = document.getElementById("tsumego");
						svg.setAttribute("viewBox",[vbarray[1],vbarray[0],vbarray[3]-unit,vbarray[2]+unit ].join(" "));
						texte = document.getElementById("toplay");
						texte.setAttribute("x",yg);
					}
					]]
				}
			}
		)
		numstr = tostring(k)
		while numstr:len() < 3 do
			numstr = "0"..numstr
		end
		file = io.open("./cho_svg/"..name.."_"..numstr..".svg","w")
		io.output(file)
		io.write(luatoxml.toxml({svg = svg}))
		io.close(file)
		csv = csv .. name.."_"..numstr..".svg,"..comment.."\n";
	end
	file = io.open("./cho_svg/"..name..".csv","w")
	io.output(file)
	io.write(csv)
	io.close(file)
end
