local re = require"re"

parserre = re.compile([[
collection <- {| (gametree space)+  |}
gametree <- space "(" node+ space ")" space
node <-  space ";"  {|{:type: ''->'node' :} property* {:nxt: (node / variation)* :}|} -> reformat
variation <- space {|{:type: ''->'variation' :} ("(" node space ")")^+2 |} space
property <- space {|{:id:  propident :} space ("[" value  "]" space)+ |} space
propident <- [A-Z]+
value <-  space { [^][]* (space [^][%s] )*}  
space <- %s*
]],{["reformat"]=
	function(t) 
		local nt={}
		for _,v in ipairs(t) do
			id = v.id
			v.id = nil
			nt[id]=v
		end
		nt.nxt = t.nxt
		nt.type = t.type
		return nt
	end
})

parser2 = re.compile([[
	collection <- {| gametree+ |}
	gametree <- space "(" {| {:mainline: sequence:} {:variation: {|gametree*|}:}|} space ")"
	sequence <- space {| node+ |}
	node <- space ";"{| property+ |}
	property <- space  {|{:id: propident:} propvalue+|} 
	propident <- [A-Z]+
	propvalue <-  space "[" space { [^][]*}  "]"
	space <- %s*
]])

simple = parser2:match("(;BP[aa][cd]WP[zz];AB[ee](;AW[ff])(;AW[ef]))")


--[[  program   = Ct(V'sexpr' ^ 0),
  wspace    = S' \n\r\t' ^ 0,
  atom      = V'boolean' + V'integer' + V'string' + V'symbol',
    symbol  = C((1 - S' \n\r\t"\'()[]{}#@~') ^ 1) /
              function(s) return _G[s] end,
    boolean = C(P'true' + P'false') /
              function(x) return x == 'true' end,
    integer = C(R'19' * R'09' ^ 0) /
              tonumber,
    string  = P'"' * C((1 - S'"\n\r') ^ 0) * P'"',
  coll      = V'list' + V'array',
    list    = P'\'(' * Ct(V'expr' ^ 1) * P')',
    array   = P'[' * Ct(V'expr' ^ 1) * P']',
  expr      = V'wspace' * (V'coll' + V'atom' + V'sexpr'),
  sexpr     = V'wspace' * P'(' * V'symbol' * Ct(V'expr' ^ 0) * P')' /
              function(f, ...) return f(...) end
}
]]--



--function tosgf(t,f)
--  local out = ";"
--  if f == nil then
--	--top of the table  
--    out = "(;"
--	sgf = t[1]
--else
--	sgf = t
--  end
--  local nxt = {}
--  for prop,val in pairs(sgf) do
--    if prop == "nxt" then
--      nxt = sgf.nxt 
--    elseif prop ~= "type" then
--      out = out .. prop .. "[" .. table.concat(val,"][").."]" 
--    end
    
--  end
--	if sgf.nxt ~=nil then
--		if sgf.nxt.type == "node" then
--			out = out .. tosgf(sgf.nxt,1)
--		elseif sgf.nxt.type == "variation" then
--			for _,var in ipairs(sgf.nxt) do
--				out = out .. "(".. tosgf(var,1) .. ")"
--			end
--		end
--	end
--  if f == nil then
--    out = out..")"
--  end
--  return out
--end

-- print contents of a table, with keys sorted. second parameter is optional, used for indenting subtables

function dump(t,indent)
    if not indent then indent = "" end
    for n,v in pairs(t) do
        local name = ""
        if type(n)~="number" then
          name = tostring(n)..":"
        end
        if type(v) == "table" then
            if(v==t) then -- prevent endless loop if table contains reference to itself
                print(indent..name.." <-")
            else
                print(indent..tostring(n)..":")
                dump(v,indent.."   ")
            end
        else
            if type(v) == "function" then
                print(indent..tostring(n).."()")
            else
                print(indent..name..tostring(v))
            end
        end
    end
end

function readsgffile(filename)
	local f = io.open(filename)
	local output = ""
	while 1 do
		local l = f:read()
		if not l then break end
		output = output .. l
	end
	return output
end



sgfexample =
[[(;FF[4]AP[Primiview:3.1]GM[1]SZ[19]GN[Gametree 1: properties]US[Arno Hollosi]

(;B[pd]N[Moves, comments, annotations]
C[Nodename set to: "Moves, comments, annotations"];W[dp]GW[1]
C[Marked as "Good for White"];B[pp]GB[2]
C[Marked as "Very good for Black"];W[dc]GW[2]
C[Marked as "Very good for White"];B[pj]DM[1]
C[Marked as "Even position"];W[ci]UC[1]
C[Marked as "Unclear position"];B[jd]TE[1]
C[Marked as "Tesuji" or "Good move"];W[jp]BM[2]
C[Marked as "Very bad move"];B[gd]DO[]
C[Marked as "Doubtful move"];W[de]IT[]
C[Marked as "Interesting move"];B[jj];
C[White "Pass" move]W[];
C[Black "Pass" move]B[tt])

(;AB[dd][de][df][dg][do:gq]
  AW[jd][je][jf][jg][kn:lq][pn:pq]
N[Setup]C[Black & white stones at the top are added as single stones.

Black & white stones at the bottom are added using compressed point lists.]
;AE[ep][fp][kn][lo][lq][pn:pq]
C[AddEmpty

Black stones & stones of left white group are erased in FF[3\] way.

White stones at bottom right were erased using compressed point list.]
;AB[pd]AW[pp]PL[B]C[Added two stones.

Node marked with "Black to play".];PL[W]
C[Node marked with "White to play"])

(;AB[dd][de][df][dg][dh][di][dj][nj][ni][nh][nf][ne][nd][ij][ii][ih][hq]
[gq][fq][eq][dr][ds][dq][dp][cp][bp][ap][iq][ir][is][bo][bn][an][ms][mr]
AW[pd][pe][pf][pg][ph][pi][pj][fd][fe][ff][fh][fi][fj][kh][ki][kj][os][or]
[oq][op][pp][qp][rp][sp][ro][rn][sn][nq][mq][lq][kq][kr][ks][fs][gs][gr]
[er]N[Markup]C[Position set up without compressed point lists.]

;TR[dd][de][df][ed][ee][ef][fd:ff]
 MA[dh][di][dj][ej][ei][eh][fh:fj]
 CR[nd][ne][nf][od][oe][of][pd:pf]
 SQ[nh][ni][nj][oh][oi][oj][ph:pj]
 SL[ih][ii][ij][jj][ji][jh][kh:kj]
 TW[pq:ss][so][lr:ns]
 TB[aq:cs][er:hs][ao]
C[Markup at top partially using compressed point lists (for markup on white stones); listed clockwise, starting at upper left:
- TR (triangle)
- CR (circle)
- SQ (square)
- SL (selected points)
- MA ('X')

Markup at bottom: black & white territory (using compressed point lists)]
;LB[dc:1][fc:2][nc:3][pc:4][dj:a][fj:b][nj:c]
[pj:d][gs:ABCDEFGH][gr:ABCDEFG][gq:ABCDEF][gp:ABCDE][go:ABCD][gn:ABC][gm:AB]
[mm:12][mn:123][mo:1234][mp:12345][mq:123456][mr:1234567][ms:12345678]
C[Label (LB property)

Top: 8 single char labels (1-4, a-d)

Bottom: Labels up to 8 char length.]

;DD[kq:os][dq:hs]
AR[aa:sc][sa:ac][aa:sa][aa:ac][cd:cj]
  [gd:md][fh:ij][kj:nh]
LN[pj:pd][nf:ff][ih:fj][kh:nj]
C[Arrows, lines and dimmed points.])

(;B[qd]N[Style & text type]
C[There are hard linebreaks & soft linebreaks.
Soft linebreaks are linebreaks preceeded by '\\' like this one >o\
k<. Hard line breaks are all other linebreaks.
Soft linebreaks are converted to >nothing<, i.e. removed.

Note that linebreaks are coded differently on different systems.

Examples (>ok< shouldn't be split):

linebreak 1 "\\n": >o\
k<
linebreak 2 "\\n\\r": >o\

k<
linebreak 3 "\\r\\n": >o\
k<
linebreak 4 "\\r": >o\
k<]

(;W[dd]N[W d16]C[Variation C is better.](;B[pp]N[B q4])
(;B[dp]N[B d4])
(;B[pq]N[B q3])
(;B[oq]N[B p3])
)
(;W[dp]N[W d4])
(;W[pp]N[W q4])
(;W[cc]N[W c17])
(;W[cq]N[W c3])
(;W[qq]N[W r3])
)

(;B[qr]N[Time limits, captures & move numbers]
BL[120.0]C[Black time left: 120 sec];W[rr]
WL[300]C[White time left: 300 sec];B[rq]
BL[105.6]OB[10]C[Black time left: 105.6 sec
Black stones left (in this byo-yomi period): 10];W[qq]
WL[200]OW[2]C[White time left: 200 sec
White stones left: 2];B[sr]
BL[87.00]OB[9]C[Black time left: 87 sec
Black stones left: 9];W[qs]
WL[13.20]OW[1]C[White time left: 13.2 sec
White stones left: 1];B[rs]
C[One white stone at s2 captured];W[ps];B[pr];W[or]
MN[2]C[Set move number to 2];B[os]
C[Two white stones captured
(at q1 & r1)]
;MN[112]W[pq]C[Set move number to 112];B[sq];W[rp];B[ps]
;W[ns];B[ss];W[nr]
;B[rr];W[sp];B[qs]C[Suicide move
(all B stones get captured)])
)

(;FF[4]AP[Primiview:3.1]GM[1]SZ[19]C[Gametree 2: game-info

Game-info properties are usually stored in the root node.
If games are merged into a single game-tree, they are stored in the node\
 where the game first becomes distinguishable from all other games in\
 the tree.]
;B[pd]
(;PW[W. Hite]WR[6d]RO[2]RE[W+3.5]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[dp]
C[Game-info:
Black: B. Lack, 5d
White: W. Hite, 6d
Place: London
Event: Go Congress
Round: 2
Result: White wins by 3.5])
(;PW[T. Suji]WR[7d]RO[1]RE[W+Resign]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[cp]
C[Game-info:
Black: B. Lack, 5d
White: T. Suji, 7d
Place: London
Event: Go Congress
Round: 1
Result: White wins by resignation])
(;W[ep];B[pp]
(;PW[S. Abaki]WR[1d]RO[3]RE[B+63.5]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[ed]
C[Game-info:
Black: B. Lack, 5d
White: S. Abaki, 1d
Place: London
Event: Go Congress
Round: 3
Result: Balck wins by 63.5])
(;PW[A. Tari]WR[12k]KM[-59.5]RO[4]RE[B+R]
PB[B. Lack]BR[5d]PC[London]EV[Go Congress]W[cd]
C[Game-info:
Black: B. Lack, 5d
White: A. Tari, 12k
Place: London
Event: Go Congress
Round: 4
Komi: -59.5 points
Result: Black wins by resignation])
))
]]

--test
sgftable = parser2:match(sgfexample)
if sgftable == nil 
then print("error in parsing sgf example") 
else 
print("parser working") 
print("sgftable")
dump(sgftable,"  ")
print(tosgf(sgftable))
--tohtml(sgftable)
end


print("simple")
dump(simple," ")
print(tosgf(simple))

test = parser2:match([[(;FF[4]SZ[19];AB[dd];AW[ee]C[go
	go](;AB[de])(;AB[ed])
	
	(;AB[gg]))]])
dump(test," ")

--dump(test.nxt," ")
print(tosgf(test))




openingsgf = 

[[(;GM[1]FF[4]CA[UTF-8]AP[CGoban:3]ST[2] RU[Japanese]SZ[19]KM[0.00] PW[sai]PB[toya koyo]CR[bd][fd][nd][rd][df][pf][bh][fh][hh][lh][nh][rh][jj][bl][fl][hl][ll][nl][rl][dn][pn][bp][fp][np][rp]LB[dd:1][gd:2][jd:3][md:4][pd:5][dh:6][gh:7][jh:8][mh:9][ph:10][dl:11][gl:12][jl:13][ml:14][pl:15][dp:16][gp:17][jp:18][mp:19][pp:20]TR[de][pe][cf][ef][of][qf][dg][pg][ji][ij][kj][jk][dm][pm][cn][en][on][qn][do][po]SQ[dc][pc][af][gf][mf][sf][jg][di][pi][gj][mj][dk][pk][jm][an][gn][mn][sn][dq][pq]C[Principles of fuseki Rule 1. Goal builds in corner-side development Rule 2. Creatively use stone Hoshi Rule 3. Find the correct hasami Rule 4. Have an intrusion counterplan Rule 5. Stone 5-4 focuses on external influence Rule 6. Development-4 line, 3-line completion Rule 7. Moyo should look like a box Good form Rule 8. Civil strife destroys resources. Rule 9. Do not allow hane to develop your stones Rule 10. Impossible to catch up if you push in front of you Rule 11. An empty triangle is a bad shape. Rule 12. Value of the grandchildren 30 points Rule 13. Do not automatically do atari Strategy Rule 14. Attack the enemy by depriving him of the base. Rule 15. Do not join the weak stones. Rule 16. Kill two birds with one stone. Rule 17. Use density to attack. Rule 18. Stay away from safe positions. Rule 19. Moyo reduce easy moves Rule 20. Do not cling to the stones that have done their work.] (;B[dd]LB[dd:1]C[Rule 1. The purpose of building in the corner - the development of the parties] (;AW[dd][op][qp]AB[oc][qd][dp]C[Boombox Position Where should Black&#39;s next move be?]PL[B] (;B[qj]LB[qj:1]C[key point for both sides] ;W[ql] ;B[oj]LB[oj:2][ql:1]C[looks like black territory]) (;B[jc] ;W[qj]LB[jc:1][qj:2]C[comparison with development in another direction] ;B[je]LB[je:1]TR[oc]C[bad balance])) (;AE[dd]AB[oc][qd]LB[jc:B][qj:A]C[And - the main direction of development from shimar B - secondary]) (;AW[dd][qj][pp]AB[oc][qd][dp]LB[qj:1]C[seizure of black development]PL[W] (;B[qh]LB[qh:1]C[still a good move]) (;W[qg]LB[qg:1][qh:A]C[also a good move])) (;AE[dd]AW[op][qp]AB[oc][qd][qj]LB[qj:1]C[Double Val]) (;AE[dd]AW[qo][pq]AB[oc][qd][qj]LB[qj:1][jq:A]C[white ratio]) (;AE[dd]AB[oc][qd][qj][pp]LB[qj:1]C[big moyo]) (;AE[dd]AW[qm][qp][kq]AB[oc][qd][qj][on][oq]LB[qj:1]C[underdevelopment]) (;AE[dd]AW[qp]AB[oc][qd] (;W[op] ;B[qj]LB[qj:2][op:1]C[obviously, but ...]) (;W[qj] ;B[op]LB[qj:1][op:2]C[not necessarily better]))) (;B[gd]LB[gd:2]C[Rule 2. Creatively use the Hoshi stone] (;AE[gd]AW[dc][fd][qf][dp]AB[pd][de][ci][pp]C[Theme position: Black move Pay special attention to the upper-right corner.]PL[B] (;B[nd] (;W[rd] ;B[qc] ;W[qi]LB[qc:3][nd:1][rd:2][qi:4]C[Strengthening] ;B[jc]LB[jc:1]TR[qc]C[probable territory]) (;W[pj]LB[qc:B][nd:2][rd:A][qf:1][pj:3]C[value of the exchange of moves] (;B[jc] ;W[qc] ;B[qd] ;W[rd] ;B[re] ;W[rc] ;B[qe] ;W[ob]LB[ob:8][jc:1][qc:2][rc:6][qd:3][rd:4][qe:7][re:5]C[no territory]) (;B[rd] ;W[lc]LB[lc:2][qc:A][rd:1]C[Don't get greedy.]) (;B[ql]LB[jc:A][ql:1]C[good strategy]))) (;B[qj]LB[qj:1]C[Hasami] (;W[of] ;B[nd]LB[nd:3][of:2][qj:1]C[perfect development]) (;W[qc] ;B[qd] ;W[pc] ;B[od] ;W[rd] ;B[re] ;W[rc] ;B[qe] ;W[nc] ;B[pf]LB[nc:9][pc:3][qc:1][rc:7][od:4][qd:2][rd:5][qe:8][re:6][pf:10]C[Assignment])) (;B[qh]LB[qh:1]C[tough hasami] ;W[qc] ;B[qd] ;W[pc] ;B[od] ;W[rd] ;B[re] ;W[rc] ;B[qe] ;W[nc]LB[nc:9][pc:3][qc:1][rc:7][od:4][qd:2][rd:5][qe:8][re:6][pf:A]C[same strategy])) (;AE[gd]AW[pb][nc][qc][jd]AB[pc][pd][pf][qj]LB[pb:3][pc:2][qc:1]C[(post-invasion)]PL[B] (;B[ob] ;W[qb] ;B[oc] ;W[re]LB[ob:1][qb:2][oc:3][re:4]C[Sharing] ;B[rf] ;W[qe] ;B[qf] ;W[pe] ;B[oe] ;W[qd] ;B[of]LB[qd:6][oe:5][pe:4][qe:2][of:7][qf:3][rf:1]C[influence]) (;B[qd] ;W[rb] ;B[rc] ;W[qb]LB[pb:1][qb:5][rb:3][rc:4][qd:2]TR[qj]C[corner territory])) (;AE[gd]AW[nc][jd]AB[pd][pf] ;W[qc] ;B[pc] ;W[pb] ;B[ob] ;W[qb] ;B[oc] ;W[re] ;B[nd]LB[ob:4][pb:3][qb:5][oc:6][pc:2][qc:1][nd:8][re:7][qj:A]C[need to divide]) (;AE[gd]AW[nc][jd]AB[pd][pf][oj][qj] ;W[qc] ;B[pc] ;W[pb] ;B[qd] ;W[rb] ;B[rc] ;W[qb]LB[pb:3][qb:7][rb:5][pc:2][qc:1][rc:6][qd:4]TR[oj][qj]C[receipt of land])) (;B[jd]LB[jd:3]C[Rule 3. Find the correct hasami] (;AE[jd]AW[dc][de][dq]AB[pd][pj][co][pp]C[Theme position: white move Focus on lower left corner]) (;AE[jd]AW[dc][de][dq]AB[co]LB[ck:A][dk:B][cl:C][dl:D][cm:E][dm:F]C[six types of hasami] (;W[ck]LB[ck:1][dk:A][cm:B]C[and development and hasami] ;B[cm] ;W[cp] ;B[eo] ;W[fp]LB[cm:1][eo:3][cp:2][fp:4]C[blacks are still in danger]) (;W[cj] ;B[cl]LB[cj:1][cl:2]C[simple development])) (;AE[jd]AW[de][cf][dq]AB[dc][cd][ce][co]LB[cd:3][ce:1][cf:2]C[What is the white strategy?] (;W[df] ;B[fc] (;W[cj] ;B[cl]LB[fc:2][df:1][cj:3][cl:4]C[not hasami]) (;W[ck]LB[ck:1]C[development one point further])) (;W[dg] ;B[fc] ;W[ck]LB[fc:2][dg:1][ck:3]C[correct joseki])) (;AE[jd]AW[oc]AB[qd]PL[B] (;B[mc]LB[mc:1]C[the toughest hasami] ;W[md] ;B[ld] ;W[nd] ;B[qg] ;W[lc] ;B[kc] ;W[lb] ;B[kd] ;W[mb]LB[lb:7][mb:9][kc:6][lc:5][kd:8][ld:2][md:1][nd:3][qg:4]C[continuous fight]) (;B[lc] ;W[of] ;B[qg]LB[lc:1][of:2][qg:3]C[more calm])) (;AE[jd]AW[pc][qd][qe][qo]AB[nd][pe][qf][pg][pq]C[choosing the right hasami]PL[B] (;B[qm]LB[qm:1]C[strongest attack] ;W[pm] ;B[ql] ;W[oo] ;B[np]LB[ql:2][pm:1][oo:3][np:4]C[advantageous attack]) (;B[qk] ;W[pp] ;B[oq] ;W[qq] ;B[qr] ;W[rq] ;B[lq]LB[qk:1][pp:2][lq:7][oq:3][qq:4][rq:6][qr:5]C[It&#39;s too tight]))) (;B[md]LB[md:4]C[Rule 4. Have an intrusion counterplan] (;AE[md]AW[jc][dd][qj][cn][cp][dq][eq]AB[oc][qd][qm][ep][fp][jp][pp][fq]LB[qm:1]C[Boombox Position Find the best development for white] (;W[qg]LB[qg:1]C[standard development]) (;W[qf] ;B[qh]LB[qf:1][qh:2]C[Greed.] (;W[of] ;B[pi] ;W[pj] ;B[oi]LB[of:1][oi:4][pi:2][pj:3]C[division into two parts]) (;W[ph] ;B[pg] ;W[qg] ;B[pi] ;W[oh] ;B[qi] ;W[of] ;B[pj]LB[of:7][pg:2][qg:3][oh:5][ph:1][pi:4][qi:6][pj:8]TR[qj]C[not connected]))) (;AE[md]AW[qn]AB[oc][qd][qp][pq] (;W[qk]LB[qj:A][qk:3][qn:1][qp:2]C[The standard]) (;W[pj]LB[pj:1][qj:A]C[line 3 and line 4] ;B[ql] ;W[pl]LB[pl:2][ql:1]C[response to invasion] ;B[pm] ;W[qm] ;B[pk] ;W[ol] ;B[qk] ;W[ok] ;B[qj] ;W[on]LB[qj:7][ok:6][pk:3][qk:5][ol:4][pm:1][qm:2][on:8]TR[pj]C[saving connections])) (;AE[md]AW[np][pp]AB[pd][qn]PL[B] (;B[pj]LB[pj:3][qn:1][np:2]C[known position]) (;B[qj]LB[qj:1][ql:A]C[not bad, but ...])) (;AE[md]AW[qj][pn][qn][po]AB[oc][qd][qo][qp][nq][pq]LB[qj:1][qk:B][ql:A]TR[pn][qn][po]C[Here the low stroke right]PL[B] (;B[ql] ;W[pl]LB[qi:D][rj:C][pk:B][qk:A][pl:2][ql:1]C[back White]) (;B[qh]LB[dd:Т]TR[qh] (;W[dd] ;B[ql] ;W[pl] ;B[qk] ;W[pk] ;B[rj] ;W[pj] ;B[qi]LB[qi:7][pj:6][rj:5][pk:4][qk:3][pl:2][ql:1]TR[qh]C[the successful invasion]) (;W[oj]LB[oj:1][pk:B][ql:A]C[gain]))) (;AE[md]AW[qo]AB[oc][qd][pq] (;W[qj]LB[qj:1]C[not too wide?] ;B[ql] ;W[no] ;B[mq] ;W[qg]LB[qg:3][ql:1][no:2]C[space for development]) (;W[qk] ;B[qm] ;W[oo] ;B[np] ;W[pl]LB[qj:A][qk:1][pl:5][qm:2][oo:3][np:4]C[more securely]) (;W[ql] ;B[qj]LB[qj:2][ql:1][no:C][oo:A][np:B][mq:D]TR[qo]C[too securely]))) (;B[pd]LB[pd:5]C[Rule 5. Stone 5-4 focuses on external influence] (;AW[dc][de][eq][pq]AB[jd][po][cp]C[Theme position: Black move Attention to the lower-right corner of the]PL[B] ;B[np]LB[np:1]C[придавливание] ;W[qo] ;B[qn] ;W[qp] ;B[pm]LB[pm:4][qn:2][qo:1][qp:3]C[glasses against the influence of the] ;W[qf] ;B[qh] ;W[qc] ;B[qd] ;W[pc] ;B[od]LB[pc:5][qc:3][od:6][qd:4][qf:1][qh:2]C[the use of impact] ;W[rd] ;B[re] ;W[rc] ;B[qe] ;W[nc] ;B[nd] ;W[mc] ;B[md]LB[mc:7][nc:5][rc:3][md:8][nd:6][rd:1][qe:4][re:2]C[a major influence]) (;AW[pq]AB[jd][po]PL[B] (;B[np]C[придавливание] (;W[nq] ;B[mp] ;W[mq] ;B[lp]LB[lp:4][mp:2][mq:3][nq:1]C[Option]) (;W[qo] ;B[pp] ;W[qp] ;B[oq]LB[qo:2][np:1][pp:3][qp:4][oq:5]C[black Variant] ;W[qq] ;B[pn] ;W[qm]LB[qm:3][pn:2][qq:1]C[developments to the left])) (;B[qq] ;W[qr] ;B[qp] ;W[or] ;B[qk] ;W[mp]LB[qk:5][mp:6][qp:3][qq:1][or:4][qr:2]C[development on the side]) (;B[oq] ;W[or] ;B[op] ;W[nq]LB[op:3][nq:4][oq:1][or:2]C[cuke outside] ;B[pr] ;W[qr] ;B[nr] ;W[ps] ;B[mq]LB[np:A][mq:5][nr:3][pr:1][qr:2][ps:4]C[ladders])) (;AW[pq]AB[qo]PL[B] ;B[op] ;W[oq] ;B[np] ;W[mq]C[the nature of the item 5-3] (;B[qj]LB[qj:1]C[necessary development]) (;B[dd] ;W[qi] (;B[qk] ;W[qf]LB[qf:3][qi:1][qk:2]C[too closely]) (;B[qg] ;W[ql]LB[qg:1][ql:2]C[immunity to attack]))) (;AE[pd]AW[dc][co]AB[ce][dq]PL[B] (;B[ed] ;W[ec] ;B[fd] ;W[gc] ;B[ck]LB[ec:2][gc:4][ed:1][fd:3][ck:5]C[coherent strategy]) (;B[ck]LB[ed:A][ck:1]C[preservation Pro Stock]))) (;B[dh]LB[dh:6]C[Rule 6. Development-4 line, 3-line completion] (;AE[dh]AW[dc][ec][cd][cf][qp][fq][oq]AB[fc][ed][fd][jd][pd][qj][dp]LB[fq:1]C[Boombox Position Attention to the upper-left corner of the]PL[B] (;B[dn] ;W[jp]LB[dn:1][jp:2]TR[cd][cf]C[Black has problems?] (;B[cj]LB[cj:1]TR[cd][cf]C[potential for development]) (;B[qm] ;W[cl]LB[cl:1]C[Floating Panel])) (;B[cn] ;W[dr] ;B[cq] ;W[iq]LB[cn:1][cq:3][iq:4][dr:2]C[pravlnoe Shimari])) (;AE[dh]AW[jp][fq]AB[dd][cj][dn][dp]LB[cj:1]C[double capacity]) (;AE[dh]AW[ec][cd][jp][fq]AB[cj][dn][dp]LB[cg:A][cj:1]C[key point]) (;AE[dh]AB[od][qd]LB[od:1]C[ueken Shimari] ;W[mc]LB[mc:1]C[weak point] ;B[pp] (;W[ob]LB[ob:1][qb:A]C[the vagina with black base]) (;W[pc] ;B[pd] ;W[ob]LB[ob:3][pc:1][pd:2]C[washing out the black base])) (;AE[dh]AW[jc]AB[od][qd]PL[B] ;B[lc]LB[lc:1][mc:A]C[weak point protection]) (;AE[dh]AW[jc]AB[oc][qd]PL[B] ;B[lc]LB[lc:1][mc:A]C[non-urgent item]) (;AE[dh]AW[jc][qi]AB[od][qd]PL[B] ;B[lc]LB[lc:1][oc:B][qg:A]) (;AE[dh]AW[jc][nc][nd][od][pp]AB[oc][pc][qd]PL[B] (;B[pe]LB[pe:1][qj:A]C[desire for development]) (;B[qf]LB[qf:1][qj:A]C[completed form])) (;AE[dh]AW[cc]AB[dp]PL[B] ;B[fd] (;W[df] ;B[jc] ;W[cj]LB[jc:3][fd:1][df:2][cj:4]C[incompleteness]) (;W[cf] ;B[jc]LB[jc:3][fd:1][cf:2][cj:A]C[completion]))) (;B[gh]LB[gh:7]C[Rule 7. Moyo should look like a box] (;AE[gh]AW[jc][dd][ql][op][qp]AB[oc][qd][qj][dp][jq]LB[ql:1]TR[qj]C[Theme position: Black move How to protect the territory]PL[W] (;B[oj]LB[oj:1]C[the form is like a box] (;W[lq] ;B[lc]LB[lc:1]C[big moyo]) (;W[mc] ;B[md] ;W[ld] ;B[me] ;W[nc] ;B[od]LB[nc:4][ld:2][md:1][od:5][me:3]TR[mc][oj]C[still moyo]) (;W[qh] ;B[oh]LB[oh:2][qh:1]TR[oj]C[no weaknesses])) (;B[pg]LB[pg:1]C[without ambition]) (;W[qh] ;B[oj] ;W[oh]LB[oh:3][qh:1][oj:2]TR[ql]C[fighting])) (;AE[gh]AW[jc][qj][po][pq]AB[oc][qd][qh][iq][lq]TR[qj] ;W[oj] ;B[oh]LB[qf:A][oh:2][oj:1]TR[qj]C[threatened with invasion]) (;AE[gh]AW[nc][jd][jp][nq]AB[pd][pf][qj][pn][pp]PL[B] ;B[oj]LB[oj:1]C[impressive density]) (;AE[gh]AW[op][qp]AB[oc][qd]PL[B] (;B[qg] ;W[ql]LB[qg:1][qj:A][ql:2]C[shyly]) (;B[qj]LB[oj:B][qj:1][ql:A]C[large scale])) (;AE[gh]AW[nc][qf]AB[pd][qj][pp]PL[B] ;B[oe] ;W[qc] ;B[qd] ;W[pc]LB[pc:4][qc:2][qd:3][oe:1]C[usual position] (;B[pg]LB[pg:1]TR[qf]C[desire to stabilize] ;W[od] ;B[pe] ;W[ne] ;B[nf] ;W[me]LB[od:1][me:5][ne:3][pe:2][nf:4]C[nice white shape] ;B[dd] ;W[rd] ;B[re] ;W[rc] ;B[qe]LB[rc:3][rd:1][qe:4][re:2]TR[qf]C[black reduced]) (;B[me]LB[me:1]TR[qf]C[game on a large scale] ;W[dd] ;B[oj]LB[oj:1]TR[qf]C[Future Dream]))) (;B[jh]LB[jh:8]C[Rule 8. Civil strife destroys resources.] (;AE[jh]AW[cc][fc][qn][co][cp][np][dq][fq]AB[pd][df][dj][ql][cn][dn][do][oo][pp]TR[qn]C[Boombox Position White&apos;s move] (;W[on] ;B[no] (;W[nn] ;B[mo]LB[nn:3][on:1][mo:4][no:2]TR[np]C[lost stone]) (;W[mp] ;B[ol] ;W[nn] ;B[mo]LB[ol:4][nn:5][on:1][mo:6][no:2][mp:3]C[restless struggle])) (;W[qq] ;B[qp] ;W[pq] ;B[rq] ;W[op] ;B[pn] ;W[rr] ;B[rp] ;W[kq]LB[pn:6][op:5][qp:2][rp:8][kq:9][pq:3][qq:1][rq:4][rr:7]C[natural option]) (;W[pm] ;B[no] ;W[pl] ;B[qk] ;W[mp]LB[qk:4][pl:3][pm:1][on:A][no:2][mp:5]C[escape opportunity])) (;AE[jh]AW[ql][qq]AB[qo][oq] ;W[pp]LB[pp:1]TR[qo][oq]C[division into two parts] (;B[po] ;W[op]LB[po:1][op:2]TR[oq]C[position deterioration] ;B[nq] ;W[np] ;B[mq] ;W[om]LB[om:4][np:2][mq:3][nq:1]C[hopelessly]) (;B[pm] ;W[op] ;B[nq] ;W[np] ;B[mq] ;W[qm] ;B[pn]LB[pm:1][qm:6][pn:7][np:4][op:2][mq:5][nq:3]C[Safety Cover])) (;AE[jh]AW[nc][jd][qn][mp][qp]AB[pd][pf][qk][on][op] ;W[pm]LB[pm:1]TR[qk]C[how should black run away?] (;B[om] ;W[pl]LB[pk:B][ol:A][pl:2][om:1]C[weakening with your own hands]) (;B[mn]LB[mn:1]TR[qk]C[change of direction]) (;B[ok] ;W[om] ;B[nn] ;W[nm] ;B[mn]LB[ok:1][nm:4][om:2][mn:5][nn:3]C[another good strategy])) (;AE[jh]AW[nc][ph][qn]AB[pd][pf][pj][np][pp]PL[B] (;B[qh] ;W[qi] ;B[qg] ;W[pi]LB[qg:3][qh:1][pi:4][qi:2]TR[pj]C[abandoning an ally] ;B[qj] ;W[ri]C[deterioration of position]) (;B[qg] ;W[qh]LB[qg:1][qh:2]TR[pj]C[still bad]) (;B[nj]LB[nf:B][nh:A][nj:1]C[the simplest solution]))) (;B[mh]LB[mh:9]C[Rule 9. Do not allow hane to develop your stones] (;AE[mh]AW[oc][dd][jd][oe][of][og][ep]AB[mc][qd][me][pf][pg][cp][pp]LB[og:1]C[Boombox Position On the right or on the left should Black play?]PL[B] (;B[mg] ;W[ph]LB[mg:1][ph:2]TR[pg]C[escape to the left] ;B[qh] ;W[qi] ;B[rh] ;W[ri]LB[qh:1][rh:3][qi:2][ri:4]C[hard to estimate losses]) (;B[ph]LB[og:1][ph:2]C[lengthen whatever it takes!] (;W[ld] ;B[md] ;W[mg] ;B[lf]LB[ld:1][md:2][lf:4][mg:3]C[grab is not easy]) (;W[oh] (;B[pi]LB[og:1][oh:3][ph:2][pi:4]C[lengthen again!]) (;B[ke] ;W[pi] ;B[qi] ;W[qj]LB[ke:1][pi:2][qi:3][qj:4]C[similar result])))) (;AE[mh]AW[qf][pj]AB[nd][pd][pp]PL[B] (;B[qk] ;W[qj] ;B[pk] ;W[ok]LB[qj:2][ok:4][pk:3][qk:1]C[starter mistake]) (;B[ql]LB[ql:1]C[discreetly])) (;AE[mh]AW[qc][qd][re]AB[pc][jd][pd][qe][qf][pj] ;W[rf] (;B[qg]LB[rf:1][qg:2]C[standard position]) (;B[dp] ;W[qg] ;B[pg] ;W[ph]LB[pg:2][qg:1][ph:3]TR[pj]C[danger] ;B[qh] ;W[rg] ;B[oh] ;W[pf]LB[pf:4][rg:2][oh:3][qh:1]C[Trouble])) (;AE[mh]AW[pq]AB[po][oq]LB[oq:1]C[cuke outside] (;W[pp] ;B[op]LB[op:2][pp:1]C[mindlessly] ;W[qo] ;B[qn]LB[qn:2][qo:1]C[futile resistance]) (;W[or] ;B[op] ;W[nq]LB[op:2][nq:3][or:1]C[right])) (;AE[mh]AW[rd][qf][qi][ql][qm][jq][mq]AB[nc][qc][pd][qk][pl][pm][po][pq] ;W[rk] (;B[qj] ;W[rj] ;B[ok] ;W[qn]LB[qj:2][rj:3][ok:4][rk:1][qn:5]C[head against the wall]) (;B[pk] ;W[qj] ;B[qn]LB[qj:3][pk:2][rk:1][qn:4]C[right]))) (;B[ph]LB[ph:10]C[Rule 10. Impossible to catch up if you push in front of you] (;AE[ph]AW[nb][ob][nc][md][qe][mf][qf][pg][do][eo][cp][pp]AB[pb][jc][oc][dd][nd][pd][qd][of][pf][fo][ep][gp]LB[og:A]C[Theme position: Black move The correct form at vybeganii]PL[B] (;B[og] ;W[ph] ;B[oh] ;W[pi]LB[og:1][oh:3][ph:2][pi:4]C[exceptional white]) (;B[ng] ;W[qi]LB[ng:1][qi:2]C[not help white])) (;AE[ph]AW[oc][oe]AB[mc][qd][pf]LB[mc:1][oe:2][pf:3]C[White&apos;s move] (;W[of] ;B[pg] ;W[og] ;B[ph]LB[of:1][og:3][pg:2][ph:4]C[poorly]) (;W[ld]LB[ld:1]C[excellent strategy])) (;AW[ph][qh][ql][po][pq]AB[oc][qd][oi][pi][qj]LB[ph:2][oi:3][pi:1]C[melee fighting] (;W[oh] ;B[ni] ;W[nh] ;B[mi]LB[mh:A][nh:3][oh:1][mi:4][ni:2]C[the initiative for black]) (;W[ng] (;B[mi] ;W[lg]LB[lg:3][ng:1][oh:A][mi:2]C[the rapid development of]) (;B[oh] ;W[og] ;B[pg] ;W[pf]LB[pf:5][ng:1][og:3][pg:4][oh:2]C[item razrezajushhij])) (;W[nh] (;B[ni] ;W[mh]LB[mh:3][nh:1][ni:2]C[Not too well.]) (;B[oh] ;W[og] ;B[ng]LB[ng:4][og:3][pg:A][nh:1][oh:2]C[danger]))) (;AE[ph]AW[oc][pe][pf]AB[qd][qe]LB[pe:1][qe:2][pf:3]C[stroke black]PL[B] (;B[qf] ;W[pg] ;B[qg] ;W[ph]LB[qf:1][pg:2][qg:3][ph:4]C[help white]) (;B[qg] (;W[pg] ;B[qh] ;W[ph] ;B[pi]LB[pg:2][qg:1][ph:4][qh:3][pi:5]C[leading jump]) (;W[qf] ;B[rf]LB[re:B][qf:2][rf:3][qg:1][rg:A]C[Dangerous?] ;W[re] (;B[rd] ;W[rg] ;B[se] ;W[qh]LB[rd:2][re:1][se:4][rg:3][qh:5]C[failure]) (;B[rg] ;W[rd] ;B[qc] ;W[rc] ;B[rb]LB[rb:6][qc:4][rc:5][rd:3][re:1][rg:2]C[Safety Cover])))) (;AE[ph]AW[oc][oe]AB[mc][qd][pf]C[stroke black]PL[B] (;B[of] ;W[ne]LB[ne:2][of:1]C[strong move?]) (;B[me] ;W[of] ;B[pg] ;W[og] ;B[ph] ;W[mg]LB[me:1][of:2][mg:6][og:4][pg:3][ph:5]C[help Black])) (;AE[ph]AW[oc][oe]AB[ld][qd][pf]C[stroke black]PL[B] (;B[of] ;W[me]LB[me:2][of:1]C[nice white shape]) (;B[mf] ;W[of] ;B[pg] ;W[og] ;B[ph]LB[mf:1][of:2][og:4][pg:3][ph:5]C[smart move]))) (;B[dl]LB[dl:11]C[Rule 11. An empty triangle is a bad shape.] (;AE[dl]AW[dc][de][ch][ej][dk][cl][qn][jq][pq]AB[oc][id][qd][cj][dj][pj][cp][ep][hq]LB[dj:2][ej:3][dk:1]C[Boombox Position How to escape black?]PL[B] (;B[di] ;W[fk]LB[di:1][fk:2]C[the ideal target]) (;B[dh] (;W[cg] ;B[ei] ;W[fj] ;B[gh]LB[cg:2][dh:1][gh:5][ei:3][fj:4]C[good form]) (;W[dg] ;B[ei] ;W[fj] ;B[eg] ;W[df] ;B[fi] ;W[gj] ;B[hh]LB[df:6][dg:2][eg:5][dh:1][hh:9][ei:3][fi:7][fj:4][gj:8]C[Option]) (;W[di] ;B[ci] ;W[ei] ;B[cg] ;W[bh] ;B[bg]LB[bg:7][cg:5][bh:6][dh:1][ci:3][di:2][ei:4]C[It is unreasonable to]))) (;AE[dl]AW[np][pp][qq]AB[po][qo] ;W[oo] (;B[pm]LB[pm:2][oo:1]C[standartnj course]) (;B[pn]LB[pn:1]C[It's too slow.])) (;AE[dl]AW[qn][np][pp][qq]AB[po][qo] ;W[oo] ;B[pn]LB[pn:2][oo:1]TR[qn]C[Necessary]) (;AE[dl]AW[oo][np][pp][qq]AB[pn][po][qo]LB[qn:A]C[Designations]) (;AE[dl]AW[oi][pi][qo][pq]AB[oc][qd][pj][qj][qm]PL[B] ;B[qi] (;W[pg] ;B[qh] ;W[ng]LB[ng:4][pg:2][qh:3][qi:1]C[hard attack]) (;W[ph]LB[ph:2][qi:1]C[without capacity])) (;AE[dl]AW[qn][op][pq][qq]AB[pp][qp]LB[op:1]C[A bad start]PL[B] (;B[pn] ;W[po] ;B[qo] ;W[oo]LB[pn:1][oo:4][po:2][qo:3]C[Terribly]) (;B[oo] ;W[po]LB[oo:1][po:2]C[captured]) (;B[po]LB[po:1]C[Severe form])) (;AE[dl]AW[qe][qf][pg]AB[pd][qd][of][pf][pp]LB[qj:A]C[black attack]PL[B] ;B[pi] ;W[og] ;B[ng]LB[ng:3][og:2][pi:1]TR[pg]C[hard attack] (;W[oi] ;B[oh] ;W[ph] ;B[nh] ;W[qi] ;B[pj] ;W[qj] ;B[pk]LB[nh:4][oh:2][ph:3][oi:1][qi:5][pj:6][qj:7][pk:8]C[bad for white]) (;W[oh] ;B[nf]LB[nf:2][oh:1]TR[pi]C[bad form]))) (;B[gl]LB[gl:12]C[Rule 12. Value of the grandchildren 30 points] (;AE[gl]AW[cc][df][qg][dj][qj][ck][dl][cn][pn][pp][qq]AB[jc][fd][od][qd][dk][cl][dp][fp][kq][nq][pr]LB[ck:4][dk:3][cl:1][dl:2]C[Boombox Position Stroke black]PL[B] (;B[dm] ;W[ek] ;B[cm]LB[ek:2][cm:3][dm:1]TR[cn]C[ponnuki power]) (;B[ek] ;W[bl] ;B[cj] ;W[cm] ;B[di]LB[di:5][cj:3][ek:1][bl:2][cm:4]TR[cl]C[ladders])) (;AE[gl]AW[nc][qn]AB[pd][pf][pj][np][pp] ;W[ph] (;B[oh] ;W[oi] ;B[pi] ;W[og] ;B[qh] ;W[nh] ;B[pg]LB[og:5][pg:8][nh:7][oh:2][ph:1][qh:6][oi:3][pi:4]C[Victim] ;W[ne]LB[ne:1]C[a strong influence]) (;B[qh] ;W[qi] ;B[pi] ;W[qg] ;B[oh] ;W[rh] ;B[pg]LB[pg:7][qg:4][oh:5][qh:1][rh:6][pi:3][qi:2]C[doubtful blockade] ;W[qj] ;B[pk] ;W[ql]LB[qj:1][pk:2][ql:3]C[Union])) (;AE[gl]AW[qf]AB[nd][pd][qj] (;W[rd] (;B[qc] ;W[qh]LB[qc:2][rd:1][qh:3]TR[qf]C[satisfactory for black]) (;B[rc] ;W[qc] ;B[qd] ;W[rb] ;B[re] ;W[sc] ;B[qe]LB[rb:5][qc:3][rc:2][sc:7][qd:4][rd:1][qe:8][re:6]C[too easy for white])) (;W[qc] ;B[qd] ;W[rd] ;B[re] ;W[rc] ;B[qe] ;W[ob]LB[ob:7][qc:1][rc:5][qd:2][rd:3][qe:6][re:4]C[standard result])) (;AE[gl]AW[jc][nc][nd][qe][qf][pg][ri]AB[oc][pd][qd][pe][of][pf][pi][pp]LB[ri:1]TR[qe][qf][pg]C[Help Us]PL[B] (;B[rj] ;W[qj] ;B[qi] ;W[rk] ;B[rh] ;W[sj] ;B[qg]LB[qg:7][rh:5][qi:3][qj:2][rj:1][sj:6][rk:4]C[success] ;W[pj] ;B[oi] ;W[nq]LB[oi:2][pj:1][nq:3]TR[qe][qf][pg][pp]C[great density]) (;B[qk]LB[qk:1]C[wiser policy])) (;AE[gl]AW[ob][nc][pc]AB[oc][od][pe]LB[ob:2][nc:4][oc:1][od:3]C[standard position]PL[B] (;B[nb] ;W[mb] ;B[pb] ;W[na] ;B[qc] ;W[nd]LB[na:4][mb:2][nb:1][pb:3][qc:5][nd:6]C[seizure of cutting stone]) (;B[pb] (;W[qb] ;B[nb] ;W[pa] ;B[mc]LB[pa:4][nb:3][pb:1][qb:2][mc:5]C[In a efficcient manner]) (;W[nb] ;B[qc]LB[nb:2][pb:1][qc:3]C[Severe form])))) (;B[jl]LB[jl:13]C[Rule 13. Do not automatically do atari] (;AE[jl]AW[dc][cd][ce][df][dp][jp][nq]AB[jd][pd][de][cf][pj][pn][pp]LB[df:1]C[Boombox Position Black move]PL[B] (;B[ef] ;W[dg]LB[ee:A][ef:1]TR[cf]C[Unhelpful]) (;B[dg] ;W[ef] ;B[cg] ;W[dj]LB[ef:2][cg:3][dg:1][dj:4]C[not protected from attack]) (;B[cg]LB[cg:1]TR[de]C[simple elongation] ;W[ef] ;B[cj]LB[ef:1][cj:2]C[Без дефектов.])) (;AE[jl]AW[qh][qj][pk]AB[pd][qf][pj][qk][pp]LB[qj:1][pk:3][qk:2]TR[qh]C[fast stabilization]PL[B] (;B[pl] ;W[ok]LB[ok:2][pl:1]TR[pj]C[bad for black]) (;B[qi] ;W[rj] ;B[pi] ;W[ri] ;B[ph] ;W[ql]LB[ph:5][pi:3][qi:1][ri:4][rj:2][ql:6]C[whites are happy]) (;B[ql]LB[ql:1]TR[pj]C[don&#39;t help whites])) (;AE[jl]AW[jp][mp][nq][qq][nr][or][pr]AB[qj][np][pp][qp][mq][oq]LB[mq:1][nr:2]C[And what's next?]PL[B] (;B[mo] ;W[lp] ;B[no]LB[mo:1][no:3][lp:2]C[Gote]) (;B[no] ;W[lp]LB[no:1][lp:2]C[sente])) (;AE[jl]AW[jq][nq][or]AB[qm][pp][oq][pr] (;W[op] ;B[pq]LB[op:1][pq:2]C[Temptation]) (;W[nr]LB[nr:1]C[whites goal] ;B[dd] ;W[qq] ;B[pq] ;W[qr] ;B[ps] ;W[ro]LB[ro:5][pq:2][qq:1][qr:3][ps:4]C[adji in the corner]))) (;B[ml]LB[ml:14]C[Rule 14. Attack the enemy by depriving him of the base.] (;AE[ml]AW[dc][oc][ce][qo][eq]AB[qd][pe][qk][cp][pq]C[Boombox Position Black move]PL[B] (;B[qp] ;W[po] ;B[np]LB[po:2][np:3][qp:1]C[standard attack] ;W[qm] ;B[ok]LB[ok:2][qm:1]C[unreliable]) (;B[np] ;W[qq] ;B[qr] ;W[pp] ;B[oq] ;W[rq] (;B[rr] ;W[qm]LB[qm:8][np:1][pp:4][oq:5][qq:2][rq:6][qr:3][rr:7]C[As arranged]) (;B[qm] ;W[rr]LB[qm:1][rr:2]C[easy life]))) (;AE[ml]AW[np][oq][pq]AB[qj][pn][pp][qq]LB[pn:2][np:1][oq:5][pq:3][qq:4]C[stabilize]PL[B] (;B[qr] ;W[jq]LB[jq:2][qr:1]C[mandatory move]) (;B[jq] ;W[qr] ;B[qp] ;W[rr]LB[qp:3][jq:1][qr:2][rr:4]C[no attack])) (;AE[ml]AW[oc][nd]AB[ld][qd][pf]LB[ld:1][nd:2][pf:3]C[mandatory move]PL[B] (;W[qb] ;B[qj]LB[qb:1][qj:2]C[provision]) (;B[pc]LB[pc:1]C[tough fight])) (;AE[ml]AW[pb][kc][nc]AB[qc][pd][pf] ;W[qh] (;B[qb]LB[qb:2][qh:1]C[important block]) (;B[dp] ;W[qb]LB[qb:1]C[swimming group] ;B[rb] ;W[rd] ;B[rc] ;W[rf]LB[rb:1][rc:3][rd:2][rf:4]C[LATE SHIFT])) (;AE[ml]AW[dc][hc][de][dn][dp][cq]AB[jc][ci][fq][iq][dr] ;W[ck] (;B[cg] ;W[cf] ;B[dg]LB[cf:3][cg:2][dg:4][eg:A][ck:1]C[narrowly but]) (;B[pd] ;W[cg] ;B[ei]LB[cg:1][ei:2]C[target for attack])) (;AE[ml]AW[dc][de][ck]AB[cm][dp]PL[B] ;B[cg] (;W[ek] ;B[cd] ;W[dd] ;B[ce] ;W[cc] ;B[df]LB[cc:6][cd:3][dd:4][ce:5][df:7][cg:1][ek:2]C[middle]) (;W[ce] ;B[eg] ;W[ek]LB[ce:1][eg:2][ek:3]C[attack]))) (;B[pl]LB[pl:15]C[Rule 15. Do not join the weak stones.] (;AE[pl]AW[dc][gc][cg][dh][ci][di][qn][cp][eq][nq]AB[oc][qd][ce][df][bg][dg][bh][ch][qj][pp]LB[pn:B][np:A][nq:1]C[Boombox Position Black move]PL[B] (;B[np] ;W[mp] ;B[no] ;W[oq] ;B[pq]LB[no:3][mp:2][np:1][oq:4][pq:5]TR[qj][qn]C[prioedinjajtes to a stronger stone]) (;B[pn] ;W[pm] ;B[on] ;W[qo] ;B[qp] ;W[ql]LB[ql:6][pm:2][on:3][pn:1][qo:4][qp:5]C[error in direction])) (;AE[pl]AW[ql][no][nq]AB[pd][pn][jp][pp]LB[om:A][no:1]C[keep Division white]PL[B] (;B[pl] ;W[pk] ;B[ol] ;W[qm] ;B[qn] ;W[qh]LB[qh:6][pk:2][ol:3][pl:1][qm:4][qn:5]C[nice white shape]) (;B[nn] ;W[mn] ;B[nm] ;W[lo] ;B[qi]LB[qi:5][nm:3][mn:2][nn:1][lo:4]TR[ql]C[Different way]) (;B[om]LB[om:1]C[do not join at all])) (;AE[pl]AW[mc][me][qf][qi]AB[kc][oc][qd][ke]PL[B] (;B[oe] ;W[mg]LB[oe:1][mg:2]TR[mc][me]C[Trivially]) (;B[pf] ;W[pg] ;B[of]LB[of:3][pf:1][pg:2]C[Phantom attack] ;W[qe] ;B[pd] ;W[mg] ;B[og]LB[pd:2][qe:1][mg:3][og:4]C[powerful attack])) (;AE[pl]AW[qn]AB[pd][pp]PL[B] ;B[qo] ;W[pn] (;B[np] ;W[qj]LB[qj:4][pn:2][qo:1][np:3]C[Kosumi cuke]) (;B[qj] ;W[op] ;B[oq] ;W[np]LB[qj:3][pn:2][qo:1][np:6][op:4][oq:5]C[attack with bad consequences])) (;AE[pl]AB[pd][pj][pp] ;W[qn] (;B[qo] ;W[pn] ;B[np]LB[pn:3][qn:1][qo:2][np:4]TR[pj]C[a suitable course]) (;B[np] ;W[rp] ;B[qq] ;W[ql]LB[ql:4][np:1][rp:2][qq:3]C[Nothing at all])) (;AE[pl]AW[qm][pp]AB[pd][qj] ;W[qf] (;B[qe] ;W[pf] ;B[nd] ;W[oj]LB[nd:4][qe:2][pf:3][qf:1][oj:5]TR[qj][qm]C[Retaliation]) (;B[nd] ;W[rd] ;B[qc] ;W[qh] ;B[oj]LB[qc:3][nd:1][rd:2][qh:4][oj:5]C[Compromising]))) (;B[dp]LB[dp:16]C[Rule 16. Kill two birds with one stone.] (;AE[dp]AW[dd][qn][dq]AB[oc][qd][pp]LB[qn:1]C[Boombox Position Black move]PL[B] (;B[np] ;W[pj]LB[pj:2][np:1]C[Unsatisfactory]) (;B[qj]LB[qj:1]C[improvement strategies])) (;AE[dp]AB[oc][qd][pp] ;W[qj] (;B[qh] ;W[qm]LB[qh:2][qj:1][qm:3]C[better for white]) (;B[qm] ;W[qg]LB[qg:2][qm:1]C[Another way is possible])) (;AE[dp]AW[pq]AB[oc][qd][qo] ;W[pl] ;B[pn] ;W[np] ;B[rq] ;W[jq]LB[pl:1][pn:2][np:3][jq:5][rq:4]C[After dzhoseki] ;B[qj]LB[pi:B][pj:A]C[dual purpose]) (;AE[dp]AW[oc]AB[kc][qd]PL[B] (;B[pc] ;W[od] (;B[pf]LB[pc:1][od:2][pf:3]C[Double]) (;B[qg]LB[pc:1][od:2][pf:A][qg:3]C[too passively])) (;B[pf] ;W[qc] ;B[rc] ;W[pd] ;B[qe] ;W[qb]LB[qb:6][qc:2][rc:3][pd:4][qe:5][pf:1]C[You have no permission])) (;AE[dp]AW[pb][kc][nc][pn][qn]AB[qc][pd][pf][pj][qo][np][pp][jq] ;W[ql] (;B[qj]LB[qj:2][ql:1]C[stronger than it seems]) (;B[dp] ;W[rj]LB[rj:1]C[too good for white])) (;AE[dp]AW[qf][qi][pm][qn][po]AB[oc][jd][qd][qo][qp][nq][pq]PL[B] (;W[oe]LB[oe:1]C[Moyo]) (;B[oe]LB[oe:1]C[distinction in stone])) (;AE[dp]AW[dc][jd][de][co][cp][dq][dr]AB[cg][dm][cn][do][fp][er]PL[W] (;B[eg]LB[eg:1]C[key point]) (;W[eg]LB[eg:1]C[fundamental difference]))) (;B[gp]LB[gp:17]C[Rule 17. Use density to attack.] (;AE[gp]AW[dc][cf][qi][qp][mq][oq][pq][qq]AB[jc][pd][pn][qo][dp][np][op][pp]C[Boombox Position Black move]PL[B] (;B[qk] ;W[qf] ;B[qe] ;W[pf] ;B[nd]LB[nd:5][qe:3][pf:4][qf:2][qk:1]C[improper use of density]) (;B[qg] ;W[ql]LB[qg:1][ql:2]C[the temptation for white] (;B[oh] ;W[oi] ;B[ni] ;W[oj] ;B[nh]LB[nh:5][oh:1][ni:3][oi:2][oj:4]C[build a Moyo]) (;B[ol] ;W[oi]LB[oi:2][ol:1]C[MISDIRECTED]))) (;AE[gp]AW[op][qp]AB[pc][od][qd][pe]PL[B] (;B[qi] ;W[qk] ;B[oi] ;W[ok]LB[oi:3][qi:1][ok:4][qk:2]C[Experiment]) (;B[qk] ;W[qi] ;B[ok] ;W[oi]LB[oi:4][qi:2][ok:3][qk:1]C[black attack])) (;AE[gp]AW[po][qo][np][pq]AB[on][pn][qn]PL[W] (;B[qj]LB[qj:1]C[not so strong]) (;W[qj]LB[qj:1]C[open to attack])) (;AE[gp]AW[ro][qp][sp][rq]AB[qm][pn][rn][po][qo][op]C[use density to attack!]) (;AE[gp]AW[mq][oq][pq]AB[qo][np][op]LB[np:3][op:1][mq:4][oq:2]C[influence of wall]PL[W] (;B[qj]LB[qj:1]C[the development of desirable]) (;W[qj]LB[qj:1][ql:A]C[not effective])) (;AE[gp]AW[dc][hc][de][cn][dn][do][fp][er]AB[jc][ci][co][cp][dq][dr]TR[ci]C[How to attack?]PL[B] (;W[ck] ;B[cg] ;W[cf] ;B[dg]LB[cf:3][cg:2][dg:4][ck:1]C[Black stabilized]) (;W[cg] ;B[cl]LB[cg:1][cl:2]C[attack from above]) (;B[cg]LB[cg:1][cl:A]C[If black plays first]))) (;B[jp]LB[jp:18]C[Rule 18. Stay away from safe positions.] (;AE[jp]AW[cc][jc][bn][cn][en][do][dp][pp][cq][eq][cr]AB[oc][qd][qj][bm][cm][dm][dn][bo][co][cp][bq][dq]TR[cc]C[Boombox Position White&apos;s move] (;W[ci] ;B[cf] ;W[ed] ;B[ef]LB[ed:3][cf:2][ef:4][ci:1]C[too wide]) (;W[ch] ;B[cf] ;W[ed] ;B[ef] ;W[ck] ;B[eh]LB[ed:3][cf:2][ef:4][ch:1][eh:6][ck:5]C[more subdued]) (;W[df]LB[df:1]TR[jc]C[the appropriate strategy] ;B[ch] ;W[dh] ;B[di] ;W[eh] ;B[cg] ;W[cf]LB[cf:6][cg:5][ch:1][dh:2][eh:4][di:3]C[Let the black territory]) (;W[cg] (;B[ci] ;W[ed]LB[ed:3][cg:1][ci:2]C[Envy]) (;B[ce] ;W[ec] ;B[ee]LB[ec:2][ce:1][ee:3][cj:A]C[the invasion was still possible]))) (;AE[jp]AW[pb][kc][nc][ql][qn][lp][qp]AB[qc][pd][pf][ln][on][op]PL[B] (;B[qj]LB[qh:A][qj:1]C[здравый смысл]) (;B[qi]LB[qi:1]C[He was deeper, more reserved, and more tactful He had an unusually good eye both for things and people.])) (;AE[jp]AW[ql][qn][lp][qp]AB[pd][ln][on][op] ;W[nc] (;B[pf]LB[nc:1][pf:2]C[Сомнительно.]) (;B[qf] ;W[pb] ;B[qc] ;W[kc]LB[pb:2][kc:4][qc:3][qf:1][qj:A]C[preservation corner])) (;AE[jp]AW[jc][oc][pe][pf][oh]AB[qd][qe][qg][qh][pp] (;W[qn] ;B[np] ;W[rp] ;B[qq] ;W[qk]LB[qk:5][qn:1][np:2][rp:3][qq:4]C[On the side]) (;W[nq] ;B[pn] ;W[jp]LB[pn:2][jp:3][nq:1]C[The right route])) (;AE[jp]AW[qb][oc][qc][pd][pq]AB[rc][kd][qd][qe][of][qh]PL[B] ;B[qo] (;W[pl] ;B[no] ;W[mq]LB[pl:2][no:3][qo:1][mq:4]C[Aggressive]) (;W[op]LB[pn:A][op:1]C[Sleep sound!]))) (;B[mp]LB[mp:19]C[Rule 19. Moyo reduce easy moves] (;AE[mp]AW[cc][jc][dd][fd][ql][po][jp][fq][pq]AB[lc][bd][od][qd][cf][ci][oj][qj][dn][dp]C[Position of the topic: White&#39;s move. What to do with my black at the top right?] (;W[qh] ;B[oh]LB[oh:2][qh:1]C[too deep]) (;W[oh] (;B[qh] ;W[lh]LB[lh:3][oh:1][qh:2]C[not deep]) (;B[mj] ;W[qh]LB[qf:C][mh:B][oh:1][qh:3][mj:2][rj:A]C[now invasion])) (;W[nh] ;B[ph]LB[nh:1][oh:A][ph:2]C[Unsatisfactory])) (;AE[mp]AW[nc][jd][jp][nq]AB[pd][pf][qj][pn][pp]PL[W] (;B[oj]LB[oj:1]C[moyo gain]) (;W[oj]LB[ph:A][qh:C][oj:1][pl:B][ql:D]C[life point])) (;AE[mp]AW[jc][mc][kq][nq][pr]AB[oc][qd][qj][pn][pp][qq]PL[B] (;W[oj]LB[qh:A][oj:1]C[perfect move]) (;B[oj]LB[oj:1]C[life point])) (;AE[mp]AW[jc][qo][lq][pq]AB[lc][od][qd][qj][qm][jq] (;W[qh]LB[qh:1]C[invitation to attack]) (;W[pi]LB[pi:1]C[more reasonable way] (;B[qi] ;W[ph] ;B[pj] ;W[ni] ;B[oj] ;W[mg]LB[mg:6][ph:2][ni:4][qi:1][oj:5][pj:3]C[bounce off easily]) (;B[pj] ;W[oi] ;B[rh] ;W[mi]LB[rh:3][mi:4][oi:2][pj:1]C[alternative]))) (;AE[mp]AW[jc][np][pp][jq]AB[lc][od][qd][qj][qn] (;W[pi] ;B[pj] ;W[oi] ;B[rh]LB[rh:4][oi:3][pi:1][pj:2][ql:A]C[Elimination]) (;W[oj]LB[oj:1][ql:A]C[right strategy]))) (;B[pp]LB[pp:20]C[Rule 20. Do not cling to the stones that have done their work.] (;AW[cc][dc][ic][nc][ed][cp][jp][eq][nq]AB[bc][cd][pd][be][pf][dg][qj][cl][pn] ;W[qc] ;B[pc] ;W[pb] ;B[qd] ;W[rb]LB[pb:3][rb:5][pc:2][qc:1][qd:4]C[Boombox Position Black move] (;B[rc] ;W[qb]LB[qb:2][rc:1][rd:A]C[atari] ;B[oj] ;W[rd] ;B[re] ;W[sc]LB[sc:3][rd:1][re:2]TR[rc]C[Gote]) (;B[oj] ;W[rd] ;B[re]LB[rd:1][re:2]C[sente for whites])) (;AW[qo][qp][rp][pq][pr][qr][rr]AB[qn][po][np][oq][qq][rq]PL[B] ;B[ro] ;W[sq]LB[ro:1][sq:2]C[limited role] (;B[rn] ;W[pn]LB[pn:2][rn:1]TR[ro]C[meaningless]) (;B[pm]LB[pm:1][rm:B][rn:A][so:C]TR[ro]C[right])) (;AW[pm]AB[pj][np] ;W[qp] ;B[qq] ;W[po] ;B[op] ;W[ro] (;B[rp] ;W[qo]LB[po:3][qo:7][ro:5][op:4][qp:1][rp:6][qq:2]C[sente but ...] ;B[dd] ;W[rq] ;B[rr] ;W[sp]LB[sp:3][rq:1][rr:2]TR[rp]C[too much]) (;B[rq]LB[rq:1]C[Goth, but ...])) (;AW[qn]PL[B] ;B[pn] ;W[pm] ;B[on] ;W[ro] ;B[rp] ;W[qp] ;B[qq] ;W[qo]LB[pm:2][on:3][pn:1][qo:8][ro:4][qp:6][rp:5][qq:7]C[where to connect?] (;B[rq] ;W[qj] ;B[np]LB[qj:2][np:3][rq:1]TR[rp]C[save one stone]) (;B[pq] ;W[rq] ;B[rr] ;W[sp]LB[sp:4][pq:1][rq:2][rr:3]TR[rp]C[help white])) (;AW[rm][rn][qo][qp][qq]AB[pd][pl][rl][qm][qn][po][pq] ;W[qk] ;B[ql]LB[qk:1][ql:2]C[tempo] ;W[qi] ;B[qg]LB[qg:2][qi:1]TR[qk]C[lack of information]) (;AW[qn][no][po][pp][nq][oq][qq][rq]AB[pn][qo][qp][mq][pq][nr][or][pr]C[black move]PL[B] (;B[op] ;W[oo] ;B[np]LB[oo:2][np:3][op:1]TR[nq][oq]C[does rather not apply]) (;B[oo]LB[oo:1]C[key stones])) (;AE[pp]AW[od][pd][ne][qe][lf][mf]AB[mc][oc][pc][md][qd][rd][me][nf]LB[nf:1]C[White&apos;s move] (;W[of] ;B[ng] ;W[og] ;B[nh]LB[of:1][ng:2][og:3][nh:4]C[tough fight]) (;W[ng] ;B[of] ;W[og] ;B[pf] ;W[pg] ;B[qf] ;W[qg] ;B[rf]LB[of:2][pf:4][qf:6][rf:8][ng:1][og:3][pg:5][qg:7]C[more reasonable choice]))))
]]


opening1 = parserre:match(openingsgf)

opening2 = parser2:match(openingsgf)

function sgf2igo(collection)
	for _,game in ipairs(collection) do
		mainline = game.mainline
		variations = game.variations
		for _,node in ipairs(mainline) do
			for _,prop in ipairs(node) do
				id = prop.id
				for _,val in ipairs(prop) do
					functionTable.id(val)
				end
			end
		end
		sgf2igo(variations)
	end
end

	
	


if opening ~= nil then
	print("opening parsed")
	print("length sgf:",string.len(openingsgf))
	openingrecom = tosgf(opening)
	print("length reomp sgf:",string.len(openingrecom))
	print(openingrecom)
else
	print("error in opening parse")
end

--function foldSgfTree(tree, fun, fusion,acc)
--	for k,subtree in ipairs(tree.variation) do
--		acc = fusion(acc,foldSgfTree(subtree,fun,fusion,)

function merge(t1,t2)
	t = {}
	for k,v in pairs(t1) do
		t[k]=v
	end
	for k,v in pairs(t2) do
		if t[k] ~= nil then
			for _,vsub in ipairs(v) do
				table.insert(t[k],vsub)
			end
		else
			t[k] = v
		end
	end
	return t
end


function extract(gametree)
	acc = {}
	for _,node in ipairs(gametree.mainline) do
		for _,prop in ipairs(node) do
			acc[prop.id] = acc[prop.id] or {}
			for _,val in ipairs(prop) do
				table.insert(acc[prop.id],val)
			end
		end
	end
	for _,subtree in ipairs(gametree.variation) do
		acc = merge(acc,extract(subtree))
	end
	return acc
end

listprop = extract(opening2[1])

sgfNotation =   "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

function readNotation(str)
	x = sgfNotation:find(str:sub(1,1))
	y = sgfNotation:find(str:sub(2,2))
	return x,y
end

function printGoban(gb)
	local text = "\\begin{tikzpicture}\n"
	sz = gb.size 
	xmin = sz
	ymin = sz
	xmax = 0
	ymax = 0
	text = text .. "\\draw[step = 1](1,1)grid("..sz..","..sz..");\n"
	text = text .. "\\draw[thick](1,1)--("..sz..",1)--("..sz..","..sz..")--(1,"..sz..")--cycle;\n"
	if sz == 19 then
		for i = 0,2 do
			for j = 0,2 do
				x = 4+6*i
				y = 4+6*j
				text = text .. "\\draw("..x..","..y..")circle(0.1);\n"
			end
		end
	end
	
	for k,v in pairs(gb) do
		if k ~= "size" then
			x,y = readNotation(k)
			xmin = math.min(x,xmin)
			xmax = math.max(x,xmax)
			ymin = math.min(y,ymin)
			ymax = math.max(y,ymax)
			addstone  = "\\draw"..v.color
			if v.symbol ~= nil then
				if v.color == "black" then
					oth = "white"
				else
					oth = "black"
				end
				
				if type(v.symbol) == "number" then
					addsymbol = "\\draw["..oth.."](0,0)node{"..v.symbol.."};"
				else
					addsymbol = "\\draw"..oth..v.symbol..";"
				end
				
			else
				addsymbol = ""
			end
			text = text .. "\\begin{scope}[shift={("..x..","..y..")}]\n"
			text = text .. addstone..addsymbol.."\n"
			text = text .. "\\end{scope}\n"
		end
	end
	if xmin < sz/2 then xmin = 0 else xmin = (sz+1)/2  end
	if xmax > sz/2 then xmax = sz+1 else xmax = (sz+1)/2 end
	if ymin < sz/2 then ymin = 0  else ymin = (sz+1)/2 end
	if ymax > sz/2 then ymax = sz+1 else ymax = (sz+1)/2  end
	text = text.."\\clip("..xmin..","..ymin..")rect("..xmax..","..ymax..");\n"
	text = text.."\\end{tikzpicture}"
	return text
end
	
			

function parsePrintOnCom(t)
	local gobanTab = {}
	
end

function sgf2classic(pos)
	local first = pos:sub(1,1)
	local second = pos:sub(2,2)
	if first >= "i" then
		first = string.char(string.byte(first)+1)
		if first >= "I" then
			first = string.char(string.byte(first)+1)
		end
	end
	local code = string.byte(second)-string.byte('a')+1
	if code < 1 then
		code = code + string.byte("A")+26
	end
	return(first..tostring(code))
end

function igoprint(beginning,arg)
	command = beginning.."{" --"\\black{"
	if type(arg)="table" then
		sep=""
		for _,coord in ipairs(arg) do
			command..sep..sgf2classic(coord)
			sep = ","
		end
		command = command .. "}"
	else
		command = command..sgf2classic(arg).."}"
	 end
	tex.print(command)
ensd


functionTable = {
["CA"]=function(enc) print("%CA: "..enc)   end
["AB"]=function(arg) --add black
	igoprint("\\black",arg)
	end
["SQ"]=function(arg)  --add square
	igoprint("\\gobansymbol[\\igosquare]",arg)
	end
["PL"]=function(pl)
	t = {}
	t.B = "Black"
	t.W = "White"
	print("%"..t[pl].." to play") 
	end
["TR"]=function(de)  --add triangle
	igoprint("\\gobansymbol[\\igotriangle]",arg)
	 end
["RU"]=function(rules)  print("%Rules: " .. rules) end
["ST"]=function(val)  
	print("%how variations should be shown")
	t = {
		[0] = "show variations of successor node & do board markup",
		[1] = "show variations of current node",
		[2] = "no (auto-) board markup",
		[3] = ""}
	print("%"..val.." : "..r[val])
	end
["LB"]=function(arg) --label
		local coords = arg:sub(1,2)
		local label =arg:sub(4)
		addSymbol(coords,label)
		command =.."\\gobansymbol{" --"\\black{"
	if type(arg)="table" then
		sep=""
		for _,coord in ipairs(arg) do
			command..sep..sgf2classic(coord)
			sep = ","
		end
		command = command .. "}"
	else
		command = command..sgf2classic(arg).."}"
	 end
	tex.print(command)
	end
["AE"]=function(dd)   --add empty
	end
["KM"]=function(komi)  print("%Komi: "..komi) end
["SZ"]=function(size) print("%Board size: "..size)  end
["W"]=function(arg)
		igoprint("\\white",arg)--white move
	end
["FF"]=function(version) print("%SGF Version: " .. version )  end
["AP"]=function(editor)   print("%Edited with: " .. editor ) end
["PW"]=function(white)  print("%White player: "..white) end
["PB"]=function(black) print("%Black player: "..black)  end
["CR"]=function(bd)   --add circle
	"\\gobansymbol[\\igocircle]"
	end
["GM"]=function(game) print("%Game type (1 for Go..): " .. game)   end
["AW"]=function(arg)  --add white
	igoprint("\\white",arg)
end

["B"]=function(arg)   --black move
	igoprint("\\black",arg)
	end
["C"]=function(comment)   tex.print(comment) end
	}